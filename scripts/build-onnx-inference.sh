#!/bin/bash

cp ./software/gemmini-rocc-tests/include/gemmini_params.h ./software/onnxruntime-riscv/onnxruntime/core/mlas/lib/systolic/systolic_params_int8.h
cd ./software/onnxruntime-riscv/
rm -rf ./build/
./build.sh --parallel --enable_training --for_firesim --config=Release --cmake_extra_defines onnxruntime_USE_SYSTOLIC=ON onnxruntime_SYSTOLIC_INT8=ON onnxruntime_SYSTOLIC_FP32=OFF
cd ./systolic_runner/imagenet_runner/
./build.sh --parallel --enable_training --for_firesim --config=Release

